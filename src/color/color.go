package color

import (
	"fmt"
)

const (
	red = iota
	green
	yellow
	cyan
)

const CLR_CYAN string = ""
const CLR_END string = ""
const CLR_YELLOW string = ""
const CLR_RED string = ""
const CLR_GREEN string = ""

var enable bool = false

func Enable() {
	enable = true
}

func Color(c int, format string, a ...interface{}) (n int, err error) {
	if !enable {
		return fmt.Printf(format, a...)
	}
	switch c {
	case red:
		fmt.Print(CLR_RED)
	case green:
		fmt.Print(CLR_GREEN)
	case yellow:
		fmt.Print(CLR_YELLOW)
	case cyan:
		fmt.Print(CLR_CYAN)
	}
	fmt.Printf(format, a...)
	return fmt.Print(CLR_END)
}

func Red(format string, a ...interface{}) (n int, err error) {
	return Color(red, format, a...)
}

func Green(format string, a ...interface{}) (n int, err error) {
	return Color(green, format, a...)
}

func Yellow(format string, a ...interface{}) (n int, err error) {
	return Color(yellow, format, a...)
}

func Cyan(format string, a ...interface{}) (n int, err error) {
	return Color(cyan, format, a...)
}
