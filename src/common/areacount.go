package common

//此部分主要是统计地区的访问请求的占比

import (
	"encoding/json"
	"fmt"
	//"sort"
	"strings"
)

//存放所有ip及请求次数
var CountIpMap = make(map[string]int64)

//存放大于3秒的ip及请求次数
var Gt3IpMap = make(map[string]int64)

func AreaCount(area string, AreaCountMap map[string]int64, ipcount int64) {
	if _, ok := AreaCountMap[area]; ok {
		AreaCountMap[area] = AreaCountMap[area] + ipcount
	} else {
		AreaCountMap[area] = 1
	}
}

func CountAreaMap(region string, AreaCountMap map[string]int64, ipcount int64) {
	switch {
	case strings.Contains(region, "北京"):
		AreaCount("北京", AreaCountMap, ipcount)
	case strings.Contains(region, "天津"):
		AreaCount("天津", AreaCountMap, ipcount)
	case strings.Contains(region, "上海"):
		AreaCount("上海", AreaCountMap, ipcount)
	case strings.Contains(region, "重庆"):
		AreaCount("重庆", AreaCountMap, ipcount)
	case strings.Contains(region, "河北"):
		AreaCount("河北", AreaCountMap, ipcount)
	case strings.Contains(region, "河南"):
		AreaCount("河南", AreaCountMap, ipcount)
	case strings.Contains(region, "云南"):
		AreaCount("云南", AreaCountMap, ipcount)
	case strings.Contains(region, "辽宁"):
		AreaCount("辽宁", AreaCountMap, ipcount)
	case strings.Contains(region, "黑龙江"):
		AreaCount("黑龙江", AreaCountMap, ipcount)
	case strings.Contains(region, "湖南"):
		AreaCount("湖南", AreaCountMap, ipcount)
	case strings.Contains(region, "安徽"):
		AreaCount("安徽", AreaCountMap, ipcount)
	case strings.Contains(region, "山东"):
		AreaCount("山东", AreaCountMap, ipcount)
	case strings.Contains(region, "新疆"):
		AreaCount("新疆", AreaCountMap, ipcount)
	case strings.Contains(region, "江苏"):
		AreaCount("江苏", AreaCountMap, ipcount)
	case strings.Contains(region, "浙江"):
		AreaCount("浙江", AreaCountMap, ipcount)
	case strings.Contains(region, "江西"):
		AreaCount("江西", AreaCountMap, ipcount)
	case strings.Contains(region, "湖北"):
		AreaCount("湖北", AreaCountMap, ipcount)
	case strings.Contains(region, "广西"):
		AreaCount("广西", AreaCountMap, ipcount)
	case strings.Contains(region, "甘肃"):
		AreaCount("甘肃", AreaCountMap, ipcount)
	case strings.Contains(region, "山西"):
		AreaCount("山西", AreaCountMap, ipcount)
	case strings.Contains(region, "内蒙古"):
		AreaCount("内蒙古", AreaCountMap, ipcount)
	case strings.Contains(region, "陕西"):
		AreaCount("陕西", AreaCountMap, ipcount)
	case strings.Contains(region, "吉林"):
		AreaCount("吉林", AreaCountMap, ipcount)
	case strings.Contains(region, "福建"):
		AreaCount("福建", AreaCountMap, ipcount)
	case strings.Contains(region, "贵州"):
		AreaCount("贵州", AreaCountMap, ipcount)
	case strings.Contains(region, "广东"):
		AreaCount("广东", AreaCountMap, ipcount)
	case strings.Contains(region, "青海"):
		AreaCount("青海", AreaCountMap, ipcount)
	case strings.Contains(region, "西藏"):
		AreaCount("西藏", AreaCountMap, ipcount)
	case strings.Contains(region, "四川"):
		AreaCount("四川", AreaCountMap, ipcount)
	case strings.Contains(region, "宁夏"):
		AreaCount("宁夏", AreaCountMap, ipcount)
	case strings.Contains(region, "海南"):
		AreaCount("海南", AreaCountMap, ipcount)
	case strings.Contains(region, "台湾"):
		AreaCount("台湾", AreaCountMap, ipcount)
	case strings.Contains(region, "香港"):
		AreaCount("香港", AreaCountMap, ipcount)
	case strings.Contains(region, "澳门"):
		AreaCount("澳门", AreaCountMap, ipcount)
	default:
		AreaCount("其他", AreaCountMap, ipcount)
		//fmt.Println(region)
	}
}

//总的请求各运营商在各地区数组
type MapCountDist struct {
	AreaName      []string `json:"areaname"`
	Dxmapvalue    []int64  `json:"dxmapvalue"`
	Ltmapvalue    []int64  `json:"ltmapvalue"`
	Ydmapvalue    []int64  `json:"ydmapvalue"`
	Othermapvalue []int64  `json:"othermapvalue"`
}

//大于3秒在各运营商在各地区数组
type Gt3MapCountDist struct {
	AreaName      []string `json:"areaname"`
	Dxmapvalue    []int64  `json:"dxmapvalue"`
	Ltmapvalue    []int64  `json:"ltmapvalue"`
	Ydmapvalue    []int64  `json:"ydmapvalue"`
	Othermapvalue []int64  `json:"othermapvalue"`
}

var areaarry = []string{"广东", "北京", "上海", "天津", "重庆", "四川", "湖南", "安徽", "山东", "江苏", "浙江", "江西", "湖北", "广西", "河北", "河南", "云南", "新疆", "辽宁", "黑龙江", "甘肃", "山西", "内蒙古", "陕西", "吉林", "福建", "贵州", "青海", "西藏", "宁夏", "海南", "台湾", "香港", "澳门", "其他"}
var dxcountarry = []int64{}
var ltcountarry = []int64{}
var ydcountarry = []int64{}
var othercountarry = []int64{}

//大于3秒的
var dxgt3countarry = []int64{}
var ltgt3countarry = []int64{}
var ydgt3countarry = []int64{}
var othergt3countarry = []int64{}

//总请求网络运营商分布map
type NetCountDist struct {
	Netmapvalue []NetCountArry `json:"netmapvalue"`
}

type NetCountArry struct {
	Name  string `json:"name"`
	Value int64  `json:"value"`
}

//大于3秒网络运营商分布map
type Gt3NetCountDist struct {
	Netmapvalue []Gt3NetCountArry `json:"gt3netmapvalue"`
}

type Gt3NetCountArry struct {
	Name  string `json:"name"`
	Value int64  `json:"value"`
}

func PrintCountMap(info string) string {

	//areaarry = make([]string, 0)
	dxcountarry = make([]int64, 0)
	ltcountarry = make([]int64, 0)
	ydcountarry = make([]int64, 0)
	othercountarry = make([]int64, 0)

	var DxCountMap = make(map[string]int64)
	var LtCountMap = make(map[string]int64)
	var YdCountMap = make(map[string]int64)
	//var JyyCountMap = make(map[string]int64)
	var OtherCountMap = make(map[string]int64)

	var netcountarr = make([]NetCountArry, 0)
	var NetCountMap = make(map[string]int64)

	var operator string
	mutex.Lock()
	defer mutex.Unlock()
	for xrip, ipcount := range CountIpMap {
		//fmt.Println(xrip, ipcount)
		if value, ok := IpdataMap[xrip]; ok {
			operator = value
		} else {
			rr := qqW.Find(xrip)
			operator = gbktoutf8(rr.Country + " " + rr.Area)
			//country = rr.Country
			IpdataMap[xrip] = operator
		}
		switch {
		case strings.Contains(operator, "电信"):
			CountAreaMap(operator, DxCountMap, ipcount)
			if _, ok := NetCountMap["电信"]; ok {
				NetCountMap["电信"] = NetCountMap["电信"] + ipcount
			} else {
				NetCountMap["电信"] = 1
			}
		case strings.Contains(operator, "联通"):
			CountAreaMap(operator, LtCountMap, ipcount)
			if _, ok := NetCountMap["联通"]; ok {
				NetCountMap["联通"] = NetCountMap["联通"] + ipcount
			} else {
				NetCountMap["联通"] = 1
			}
		case strings.Contains(operator, "移动"):
			CountAreaMap(operator, YdCountMap, ipcount)
			if _, ok := NetCountMap["移动"]; ok {
				NetCountMap["移动"] = NetCountMap["移动"] + ipcount
			} else {
				NetCountMap["移动"] = 1
			}
		case strings.Contains(operator, "局域网"):
			//CountAreaMap(operator, JyyCountMap, ipcount)
			if _, ok := NetCountMap["局域网"]; ok {
				NetCountMap["局域网"] = NetCountMap["局域网"] + ipcount
			} else {
				NetCountMap["局域网"] = 1
			}
		default:
			CountAreaMap(operator, OtherCountMap, ipcount)
			if _, ok := NetCountMap["其他"]; ok {
				NetCountMap["其他"] = NetCountMap["其他"] + ipcount
			} else {
				NetCountMap["其他"] = 1
			}
		}
	}

	if info == "area" {
		//for k, _ := range DxCountMap {
		//	areaarry = append(areaarry, k)
		//}

		//sort.Strings(areaarry)
		for _, i := range areaarry {
			dxcountarry = append(dxcountarry, DxCountMap[i])
			ltcountarry = append(ltcountarry, LtCountMap[i])
			ydcountarry = append(ydcountarry, YdCountMap[i])
			othercountarry = append(othercountarry, OtherCountMap[i])
		}

		aaa := &MapCountDist{
			AreaName:      areaarry,
			Dxmapvalue:    dxcountarry,
			Ltmapvalue:    ltcountarry,
			Ydmapvalue:    ydcountarry,
			Othermapvalue: othercountarry,
		}
		jsonStr, err := json.Marshal(aaa)
		if err != nil {
			fmt.Println(err)
		}
		//fmt.Printf("%s", jsonStr)
		return fmt.Sprintf("%s", jsonStr)
	} else {
		for k, v := range NetCountMap {
			netcountarr = append(netcountarr, *&NetCountArry{k, v})
		}
		aaa := &NetCountDist{
			Netmapvalue: netcountarr,
		}
		jsonStr, err := json.Marshal(aaa)
		if err != nil {
			fmt.Println(err)
		}
		//fmt.Printf("%s", jsonStr)
		return fmt.Sprintf("%s", jsonStr)
	}
}

func PrintGt3CountMap(info string) string {

	//areaarry = make([]string, 0)
	dxgt3countarry = make([]int64, 0)
	ltgt3countarry = make([]int64, 0)
	ydgt3countarry = make([]int64, 0)
	othergt3countarry = make([]int64, 0)

	var DxGt3CountMap = make(map[string]int64)
	var LtGt3CountMap = make(map[string]int64)
	var YdGt3CountMap = make(map[string]int64)
	//var JyyCountMap = make(map[string]int64)
	var OtherGt3CountMap = make(map[string]int64)

	var netGt3countarr = make([]Gt3NetCountArry, 0)
	var NetGt3CountMap = make(map[string]int64)

	var operator string
	mutex.Lock()
	defer mutex.Unlock()
	for xrip, ipcount := range Gt3IpMap {
		//fmt.Println(xrip, ipcount)
		if value, ok := IpdataMap[xrip]; ok {
			operator = value
		} else {
			rr := qqW.Find(xrip)
			operator = gbktoutf8(rr.Country + " " + rr.Area)
			//country = rr.Country
			IpdataMap[xrip] = operator
		}
		switch {
		case strings.Contains(operator, "电信"):
			CountAreaMap(operator, DxGt3CountMap, ipcount)
			if _, ok := NetGt3CountMap["电信"]; ok {
				NetGt3CountMap["电信"] = NetGt3CountMap["电信"] + ipcount
			} else {
				NetGt3CountMap["电信"] = 1
			}
		case strings.Contains(operator, "联通"):
			CountAreaMap(operator, LtGt3CountMap, ipcount)
			if _, ok := NetGt3CountMap["联通"]; ok {
				NetGt3CountMap["联通"] = NetGt3CountMap["联通"] + ipcount
			} else {
				NetGt3CountMap["联通"] = 1
			}
		case strings.Contains(operator, "移动"):
			CountAreaMap(operator, YdGt3CountMap, ipcount)
			if _, ok := NetGt3CountMap["移动"]; ok {
				NetGt3CountMap["移动"] = NetGt3CountMap["移动"] + ipcount
			} else {
				NetGt3CountMap["移动"] = 1
			}
		case strings.Contains(operator, "局域网"):
			//CountAreaMap(operator, JyyCountMap, ipcount)
			if _, ok := NetGt3CountMap["局域网"]; ok {
				NetGt3CountMap["局域网"] = NetGt3CountMap["局域网"] + ipcount
			} else {
				NetGt3CountMap["局域网"] = 1
			}
		default:
			CountAreaMap(operator, OtherGt3CountMap, ipcount)
			if _, ok := NetGt3CountMap["其他"]; ok {
				NetGt3CountMap["其他"] = NetGt3CountMap["其他"] + ipcount
			} else {
				NetGt3CountMap["其他"] = 1
			}
		}
	}

	if info == "area" {
		//for k, _ := range DxCountMap {
		//	areaarry = append(areaarry, k)
		//}

		//sort.Strings(areaarry)
		for _, i := range areaarry {
			dxgt3countarry = append(dxgt3countarry, DxGt3CountMap[i])
			ltgt3countarry = append(ltgt3countarry, LtGt3CountMap[i])
			ydgt3countarry = append(ydgt3countarry, YdGt3CountMap[i])
			othergt3countarry = append(othergt3countarry, OtherGt3CountMap[i])
		}

		aaa := &Gt3MapCountDist{
			AreaName:      areaarry,
			Dxmapvalue:    dxgt3countarry,
			Ltmapvalue:    ltgt3countarry,
			Ydmapvalue:    ydgt3countarry,
			Othermapvalue: othergt3countarry,
		}
		jsonStr, err := json.Marshal(aaa)
		if err != nil {
			fmt.Println(err)
		}
		//fmt.Printf("%s", jsonStr)
		return fmt.Sprintf("%s", jsonStr)
	} else {
		for k, v := range NetGt3CountMap {
			netGt3countarr = append(netGt3countarr, *&Gt3NetCountArry{k, v})
		}
		aaa := &Gt3NetCountDist{
			Netmapvalue: netGt3countarr,
		}
		jsonStr, err := json.Marshal(aaa)
		if err != nil {
			fmt.Println(err)
		}
		//fmt.Printf("%s", jsonStr)
		return fmt.Sprintf("%s", jsonStr)
	}
}
